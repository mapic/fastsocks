import asyncio
from Core.ClientPipMgr import ClientPipMgr
from Core.SOCKS5Mgr import SOCKS5Mgr
from Core.SOCKS5Protocal import SOCKS5Protocal
from Core import Configs

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    pipmgr = ClientPipMgr(loop)
    socks5mgr = SOCKS5Mgr()
    pipmgr.setsock5mgr(socks5mgr)
    pipmgr.start_enough_pips()

    coro = loop.create_server(lambda: SOCKS5Protocal(socks5mgr, pipmgr), Configs.CLIENT_HOST, Configs.CLIENT_PORT)
    server = loop.run_until_complete(coro)
    print('Listen on {}'.format(server.sockets[0].getsockname()))

    loop.run_forever()

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()
