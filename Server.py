import asyncio
from Core.PipProtocal import PipProtocal
from Core.ServerPipMgr import ServerPipMgr
from Core.LinkMgr import LinkMgr
from Core import Configs

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    pipmgr = ServerPipMgr(loop)
    linkmgr = LinkMgr(pipmgr, loop)
    pipmgr.setlinkmgr(linkmgr)

    coro = loop.create_server(lambda: PipProtocal(pipmgr, loop), "0.0.0.0", Configs.SERVER_PORT)
    server = loop.run_until_complete(coro)
    print('Server on {}'.format(server.sockets[0].getsockname()))

    loop.run_forever()

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()