import asyncio
from Core import Configs
from Core import Message
from Core.DataAssembler import DataAssembler

import math
import binascii


class PipMgrBase:
    def __init__(self, loop=None):
        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop
        self._pips = {}
        self._data_writes_sum = {}
        self._data_to_send = []
        self._rdatas = {}
        self._pip_data_info = {}
        self._data_assembler = DataAssembler(self._loop)
        self._pip_cycle_index = 0

    def register(self, pip):
        # print("register pip:", pip)
        self._pips[pip.get_transport()] = pip
        self._data_writes_sum[pip] = 0

    def unregister(self, pip):
        if pip.get_transport() in self._pips:
            # print("unregister pip:", pip)
            del self._pips[pip.get_transport()]
        if pip in self._data_writes_sum:
            del self._data_writes_sum[pip]

    def send_data_in_chunk(self, toport, data, orderid):
        chunkcount = math.ceil(len(data) / Configs.DATA_CHUNK_SIZE)
        if chunkcount == 0:
            chunkcount = 1
        dataid = binascii.crc32(data)
        for i in range(0, chunkcount):
            cdata = data[i * Configs.DATA_CHUNK_SIZE: (i + 1) * Configs.DATA_CHUNK_SIZE]
            msg = Message.PipDataMsg(toport, len(cdata), orderid, dataid, chunkcount, i)
            # print("PipMgrBase.send_data_in_chunk():", msg)
            self.write_data(Message.composeobjdata(msg) + cdata)

    def write_data(self, data):
        pip = None
        for p in self._pips.values():
            if p.is_can_writing():
                if pip is None or self._data_writes_sum[p] < minsum:
                    pip = p
                    minsum = self._data_writes_sum[p]

        if pip is not None:
            pip.write_data(data)
            self._data_writes_sum[pip] += len(data)
            # print("write_data,", pip, ":", data)
        else:
            self.add_remain_data(data)

    def on_pip_idle(self, pip):
        if len(self._data_to_send) > Configs.MAX_DATA_COUNT_WAIT_SENDING:
            # print("To many data to wait sending, clear the datas!")
            self._data_to_send.clear()
        elif len(self._data_to_send) > 0:
            pip.write_data(self._data_to_send.pop(0))

    def add_remain_data(self, data, pos=-1):
        self._data_to_send.insert(pos, data)

    def write_remain_data_later(self, delay=Configs.RETRY_SEND_DATA_TIME):
        if len(self._data_to_send) > Configs.MAX_DATA_COUNT_WAIT_SENDING:
            # print("To many data to wait sending, clear the datas!")
            self._data_to_send.clear()
        elif len(self._data_to_send) > 0:
            for data in self._data_to_send:
                self._loop.call_later(delay, self.write_data, data)
            self._data_to_send.clear()

    def start_enough_pips(self):
        need = Configs.MAX_PIP_COUNT - len(self._pips)
        for i in range(0, need):
            self.create_pip()

    def data_received(self, pip, data):
        # print("data_received,", pip, ":", data)
        if pip in self._rdatas:
            data = self._rdatas[pip] + data
        if pip in self._pip_data_info:
            msg = self._pip_data_info[pip]
            leftdata = data
        else:
            ret = Message.getobjfromdata(data)
            if ret is not None:
                msg = ret[0]
                leftdata = ret[1]
            else:
                msg = None
                leftdata = None
        if msg is not None:
            while msg is not None:
                if isinstance(msg, Message.PipDataMsg):
                    if len(leftdata) >= msg.datalen:
                        if pip in self._pip_data_info:
                            del self._pip_data_info[pip]
                        datas_info = self._data_assembler.recive_data(msg, leftdata[0:msg.datalen])
                        if datas_info is not None:
                            for info in datas_info:
                                self.onpipdata(info.msg, info.data)
                        leftdata = leftdata[msg.datalen:]
                    else:
                        self._pip_data_info[pip] = msg
                else:
                    # print("PipMgrBase.data_received():", msg)
                    self.onmessage(msg)

                ret = Message.getobjfromdata(leftdata)
                if ret is not None:
                    msg = ret[0]
                    leftdata = ret[1]
                else:
                    msg = None

            self._rdatas[pip] = leftdata
        else:
            self._rdatas[pip] = data

    def clear_data_assemble_by_port(self, port):
        self._data_assembler.clear_data_by_port(port)

    def init_data_assemble_by_port(self, port):
        self._data_assembler.init_data_orderid_by(port)

    def create_pip(self):
        pass

    def onmessage(self, msg):
        pass

    def onpipdata(self, msg, data):
        pass
