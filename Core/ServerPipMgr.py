from Core.PipMgrBase import PipMgrBase
from Core import Message


class ServerPipMgr(PipMgrBase):
    def __init__(self, loop=None):
        PipMgrBase.__init__(self, loop)

    def create_pip(self):
        pass

    def onmessage(self, msg):
        if isinstance(msg, Message.ConnectionRequestMsg):
            self.getlinkmgr().makelink(msg.clientport, msg.desthost, msg.destport)
        elif isinstance(msg, Message.ConnectionClosedMsg):
            self.getlinkmgr().closelink_by_request_id(msg.port)

    def onpipdata(self, msg, data):
        if msg.datalen == 0:
            self.getlinkmgr().write_eof(msg.toport)
        else:
            self.getlinkmgr().write_data(msg.toport, data)

    def setlinkmgr(self, linkmgr):
        self._linkmgr = linkmgr

    def getlinkmgr(self):
        return self._linkmgr
