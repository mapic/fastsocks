import asyncio
from Core.PipMgrBase import PipMgrBase
from Core.PipProtocal import PipProtocal
from Core import Configs
from Core import Message


class ClientPipMgr(PipMgrBase):
    def __init__(self, loop=None):
        PipMgrBase.__init__(self, loop)

    def create_pip(self):
        coro = self._loop.create_connection(lambda: PipProtocal(self, self._loop),
                                            Configs.SERVER_ADDRESS,
                                            Configs.SERVER_PORT)
        asyncio.ensure_future(coro)

    def onmessage(self, msg):
        if isinstance(msg, Message.ConnectionRespondMsg):
            self.getsock5mgr().onmessage(msg.toport, msg)
        elif isinstance(msg, Message.ConnectionClosedMsg):
            self.getsock5mgr().close(msg.port)

    def onpipdata(self, msg, data):
        if msg.datalen == 0:
            self.getsock5mgr().write_eof(msg.toport)
        else:
            self.getsock5mgr().write_data(msg.toport, data)

    def setsock5mgr(self, sock5mgr):
        self._sock5mgr = sock5mgr

    def getsock5mgr(self):
        return self._sock5mgr
