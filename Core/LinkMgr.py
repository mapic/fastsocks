import asyncio
from Core.LinkProtocal import LinkProtocal
import binascii


class LinkMgr:
    def __init__(self, pipmgr, loop=None):
        self._pipmgr = pipmgr
        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop
        self._links = {}  # local port -> LinkProtocal object

    def register(self, link):
        # print("register link:", link)
        self._links[link.getsockport()] = link

    def unregister(self, link):
        if link.getsockport() in self._links:
            # print("unregister link:", link)
            del self._links[link.getsockport()]

    def makelink(self, requestid, destaddr, destport):
        cr = self._loop.create_connection(
            lambda: LinkProtocal(requestid, self, self._pipmgr, self._loop),
            destaddr,
            destport)
        asyncio.ensure_future(cr)

    def closelink(self, sockport):
        if sockport in self._links:
            link = self._links[sockport]
            del self._links[sockport]
            link.close()

    def closelink_by_request_id(self, requestid):
        for v in self._links.values():
            if v.get_requestid() == requestid:
                v.close()
                break

    def write_data(self, sockport, data):
        if sockport in self._links:
            link = self._links[sockport]
            link.write_data(data)
        else:
            print("missing link:", binascii.crc32(data))

    def write_eof(self, sockport):
        if sockport in self._links:
            self._links[sockport].write_eof()
