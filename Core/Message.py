import pickle
import random

ENDING = 'big'


def serialize(obj):
    return pickle.dumps(obj)


def deserialize(data):
    try:
        return pickle.loads(data)
    except pickle.UnpicklingError:
        pass


def getdata(data):
    """
    Extract msg data from data
    :param data: bytearray data
    :return: If msg contanin in data, return (msgdata,remaindata), otherwise return None
    """
    if len(data) >= 5:
        msglen = int.from_bytes(data[1:5], ENDING)
        if len(data) >= msglen:
            return data[5:msglen], data[msglen:]


def getobjfromdata(data):
    """
    Get a object from data.
    :param data:
    :return: If success, return (obj, remaindata), otherwise return None
    """
    objdata = getdata(data)
    if objdata is not None:
        return deserialize(objdata[0]), objdata[1]


def composedata(data):
    """
    Composing a raw data form sending.
    :param data: data to composed
    :return:New composed data
    """
    return bytes([random.randint(0, 255)]) + int.to_bytes(5 + len(data), 4, ENDING) + data


def composeobjdata(obj):
    """
    Same to composedata, but firset serialize the obj to data
    :param obj: Object to serialize and composed to data
    :return: Composed data
    """
    return composedata(serialize(obj))


class ConnectionRequestMsg:
    def __init__(self, desthost, destport, clientport):
        self.desthost = desthost
        self.destport = destport
        self.clientport = clientport

    def __str__(self):
        return str.format("ConnectionRequestMsg: desthost={}, destport={}, clientport={}",
                          self.desthost, self.destport, self.clientport)


class ConnectionRespondMsg:
    def __init__(self, toport, serverport):
        self.toport = toport
        self.serverport = serverport

    def __str__(self):
        return str.format("ConnectionRespondMsg: toport={}, serverport={}",
                          self.toport, self.serverport)


class PipDataMsg:
    """
    A msg def data info and how mush data to recive after recive this message.
    """
    def __init__(self, toport, datalen, orderid, dataid=0, totalchunk=1, chunkindex=0):
        self.toport = toport
        self.datalen = datalen
        self.orderid = orderid
        self.dataid = dataid
        self.totalchunk = totalchunk
        self.chunkindex = chunkindex

    def __str__(self):
        return str.format("PipDataMsg: toport={}, datalen={}, orderid={}, dataid={}, totalchunk={}, chunkindex={}",
                          self.toport, self.datalen, self.orderid, self.dataid, self.totalchunk, self.chunkindex)


class ConnectionClosedMsg:
    def __init__(self, port):
        self.port = port
