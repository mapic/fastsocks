import os

MAX_PIP_COUNT = 64
RETRY_SEND_DATA_TIME = 1
MAX_DATA_COUNT_WAIT_SENDING = 100

SERVER_ADDRESS = os.getenv("server_addr", "127.0.0.1")
SERVER_PORT = os.getenv("server_port", 9999)

CLIENT_HOST = "127.0.0.1"
CLIENT_PORT = 1088

DATA_CHUNK_SIZE = 50 * 1024

WAIT_CHUNK_SECOND = 10

BEGIN_ORDER_ID = 1
