import asyncio
from Core import Message
from Core import Configs
import binascii


class SOCKS5Protocal(asyncio.Protocol):
    STATE_HANDHAKE = 1
    WAIT_REQUEST = 2
    PROCESS_REQUEST = 3
    TRANSPORT_DATA = 4

    def __init__(self, sock5mgr, pipmgr):
        self._sock5mgr = sock5mgr
        self._pipmgr = pipmgr
        self._transport = None
        self._pendingclose = False
        self._server_port = None
        self._is_write_close = False
        self._is_read_close = False

    def check_close(self):
        if self._is_read_close and self._is_write_close and self._transport is not None:
            print("check_close: ", self.getsockport())
            self.close()

    def connection_made(self, transport):
        if self._pendingclose:
            transport.close()
        else:
            self._transport = transport
            self._state = SOCKS5Protocal.STATE_HANDHAKE
            self._r_data = None
            self._sockname = transport.get_extra_info('peername')
            self._order_id = Configs.BEGIN_ORDER_ID
            self._pipmgr.init_data_assemble_by_port(self.getsockport())
            self._sock5mgr.register(self)

    def eof_received(self):
        print("eof_received: ", self.getsockport())
        self.data_received(b'')
        self._is_read_close = True
        if self.check_close():
            return False
        return True

    def connection_lost(self, exc):
        print("SOCKS5Protocal connection_lost:", self.getsockport())
        msg = Message.ConnectionClosedMsg(self.getsockport())
        data = Message.composeobjdata(msg)
        self._pipmgr.write_data(data)
        self._pipmgr.clear_data_assemble_by_port(self.getsockport())
        self._sock5mgr.unregister(self)

    def close(self):
        if self._transport is not None:
            self._transport.close()
        else:
            self._pendingclose = True

    def data_received(self, data):
        if self._r_data is not None:
            data = self._r_data + data
            self._r_data = data
        if self._state == SOCKS5Protocal.STATE_HANDHAKE:
            # +----+----------+----------+
            # |VER | NMETHODS | METHODS  |
            # +----+----------+----------+
            # | 1  |    1     | 1 to 255 |
            # +----+----------+----------+
            if len(data) >= 3 and len(data) == data[1] + 2:
                # +----+--------+
                # |VER | METHOD |
                # +----+--------+
                # | 1  |   1    |
                # +----+--------+
                self._transport.write(b'\x05\x00')
                self._state = SOCKS5Protocal.WAIT_REQUEST
                self._r_data = None
        elif self._state == SOCKS5Protocal.WAIT_REQUEST:
            # +----+-----+-------+------+----------+----------+
            # |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
            # +----+-----+-------+------+----------+----------+
            # | 1  |  1  | X'00' |  1   | Variable |    2     |
            # +----+-----+-------+------+----------+----------+
            if len(data) >= 5:
                if data[1] != 0x01 or (data[3] != 0x01 and data[3] != 0x03):
                    # Only support CONNECT
                    # Only support IPV4 or DOMAINNAME
                    self._transport.close()
                    return
                elif (data[3] == 0x01 and len(data) == 10) or (data[3] == 0x03 and len(data) == 7 + data[4]):
                    if data[3] == 0x01:
                        # IPV4
                        self._destaddr = "%d.%d.%d.%d" % (data[4], data[5], data[6], data[7])
                    else:
                        # DOMAIN
                        self._destaddr = data[5:5+data[4]].decode()
                    self._port = int.from_bytes(data[-2:], "big")
                    self._state = SOCKS5Protocal.PROCESS_REQUEST
                    self._r_data = None
                    msg = Message.ConnectionRequestMsg(self._destaddr, self._port, self.getsockport())
                    data = Message.composeobjdata(msg)
                    # print("SOCKS5Protocla.data_received()", msg)
                    self._pipmgr.write_data(data)
        elif self._state == SOCKS5Protocal.TRANSPORT_DATA:
            # print("socks5 recived, from port ", self.getsockport(), ":", data)
            self._pipmgr.send_data_in_chunk(self._server_port, data, self._order_id)
            self._order_id += 1

    def getsockname(self):
        return self._sockname

    def getsockport(self):
        return self._sockname[1]

    def write_data(self, data):
        self._transport.write(data)

    def write_eof(self):
        print("write_eof:", self.getsockport())
        # self._transport.write_eof()
        self._is_write_close = True
        self.check_close()

    def get_server_port(self):
        return self._server_port

    def on_respond(self, msg):
        if isinstance(msg, Message.ConnectionRespondMsg) and self._state == SOCKS5Protocal.PROCESS_REQUEST:
            # +----+-----+-------+------+----------+----------+
            # |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
            # +----+-----+-------+------+----------+----------+
            # | 1  |  1  | X'00' |  1   | Variable |    2     |
            # +----+-----+-------+------+----------+----------+
            data = b'\x05\x00\x00\x03' + \
                   bytearray((len(Configs.SERVER_ADDRESS),)) + \
                   Configs.SERVER_ADDRESS.encode() + \
                   int.to_bytes(msg.serverport, 2, 'big')
            self.write_data(data)
            self._r_data = None
            self._server_port = msg.serverport
            self._state = SOCKS5Protocal.TRANSPORT_DATA
