import binascii


class SOCKS5Mgr:
    def __init__(self):
        self._socks5s = {}

    def register(self, socks5):
        self._socks5s[socks5.getsockport()] = socks5

    def unregister(self, socks5):
        if socks5.getsockport() in self._socks5s:
            del self._socks5s[socks5.getsockport()]

    def close(self, port):
        if port in self._socks5s:
            self._socks5s[port].close()

    def close_by_serverport(self, port):
        for v in self._socks5s.values():
            if v.get_server_port() == port:
                v.close()
                break

    def onmessage(self, toport, msg):
        if toport in self._socks5s:
            sock5 = self._socks5s[toport]
            sock5.on_respond(msg)

    def write_data(self, toport, data):
        if toport in self._socks5s:
            sock5 = self._socks5s[toport]
            # print("SOCKs5mgr write_data toport ", toport, ":", data)
            sock5.write_data(data)
        else:
            print("SOCKs5mgr write_data - missing toport:", binascii.crc32(data))

    def write_eof(self, port):
        if port in self._socks5s:
            self._socks5s[port].write_eof()
