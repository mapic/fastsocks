from Core import Configs
import asyncio


class DataInfo:
    def __init__(self, msg, data):
        self.msg = msg
        self.data = data


class DataAssembler:
    def __init__(self, loop=None):
        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop
        self._next_id = {}  # toport -> next_order_id_infos
        self._datas = {}  # dataid -> datas
        self._timers = {}  # dataid -> timer

    def recive_data(self, msg, data):
        # print("DataAssembler.recive_data,", msg, "-", data)
        if msg.dataid in self._datas or msg.totalchunk == 1:
            rd = None
            if msg.totalchunk == 1:
                rd = data
            else:
                cds = self._datas[msg.dataid]
                cds[msg.chunkindex] = data
                iscompleted = True
                for d in cds:
                    if d is None:
                        iscompleted = False
                        break
                if iscompleted:
                    self.cancle_timer(msg.dataid)
                    rd = b''.join(cds)

            if rd is not None:
                if msg.dataid in self._datas:
                    del self._datas[msg.dataid]
                if msg.toport not in self._next_id:
                    print("missing to port %d in data assembler" % (msg.toport,))
                elif msg.orderid == self._next_id[msg.toport]["id"]:
                    # print("%d port - orderid %d" % (msg.toport, msg.orderid))
                    ret = self.composedata(msg, rd, self._next_id[msg.toport]["datas"])
                    self._next_id[msg.toport]["id"] = ret[1]
                    return ret[0]
                else:
                    # print("%d port - orderid %d, wait orderid %d" % (msg.toport, msg.orderid, self._next_id[msg.toport]["id"]))
                    self._next_id[msg.toport]["datas"][msg.orderid] = DataInfo(msg, data)
        else:
            cds = [None] * msg.totalchunk
            cds[msg.chunkindex] = data
            self._datas[msg.dataid] = cds
            handle = self._loop.call_later(Configs.WAIT_CHUNK_SECOND * msg.totalchunk, self.timer_out, msg.dataid)
            self._timers[msg.dataid] = handle

    def clear_data_by_port(self, port):
        # print("clear_data_by_port:", port)
        if port in self._next_id:
            del self._next_id[port]

    def init_data_orderid_by(self, port):
        # print("init_data_orderid_by:", port)
        self._next_id[port] = {"id": Configs.BEGIN_ORDER_ID, "datas": {}}

    def timer_out(self, dataid):
        print("timer_out: ", dataid)
        if dataid in self._datas:
            del self._datas[dataid]
        if dataid in self._timers:
            del self._timers[dataid]

    def cancle_timer(self, dataid):
        if dataid in self._timers:
            self._timers[dataid].cancel()
            del self._timers[dataid]

    @staticmethod
    def composedata(current_msg, current_data, cacheddata):
        orderid = current_msg.orderid
        oid = orderid - 1
        datas = []
        while oid in cacheddata:
            datas.append(cacheddata[oid])
            del cacheddata[oid]
            oid -= 1
        datas.append(DataInfo(current_msg, current_data))
        oid = orderid + 1
        while oid in cacheddata:
            datas.append(cacheddata[oid])
            del cacheddata[oid]
            oid += 1

        return datas, oid
