from Core import Message
from Core import Configs
import asyncio
import binascii


class LinkProtocal(asyncio.Protocol):
    def __init__(self, requestid, linkmgr, server_pipmgr, loop=None):
        """
        :param requestid: The request clint port number
        :param linkmgr:
        :param server_pipmgr:
        :param loop:
        """
        self._requestid = requestid
        self._linkmgr = linkmgr
        self._server_pipmgr = server_pipmgr
        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop
        self._order_id = Configs.BEGIN_ORDER_ID
        self._transport = None
        self._pendingclose = False
        self._is_write_close = False
        self._is_read_close = False

    def check_close(self):
        if self._is_read_close and self._is_write_close and self._transport is not None:
            print("check_close: ", self.getsockport())
            self.close()
            return True

    def connection_made(self, transport):
        if self._pendingclose:
            transport.close()
        else:
            self._transport = transport
            self._sockname = transport.get_extra_info('socket').getsockname()
            self._server_pipmgr.init_data_assemble_by_port(self.getsockport())
            self._linkmgr.register(self)

            msg = Message.ConnectionRespondMsg(self.get_requesting_client_port(), self.getsockport())
            data = Message.composeobjdata(msg)
            self._server_pipmgr.write_data(data)

    def eof_received(self):
        print("eof_received: ", self.getsockport())
        self.data_received(b'')
        self._is_read_close = True
        if self.check_close():
            return False
        return True

    def connection_lost(self, exc):
        print("LinkProtocal connection_lost:", self.getsockport())
        msg = Message.ConnectionClosedMsg(self.get_requestid())
        data = Message.composeobjdata(msg)
        self._server_pipmgr.write_data(data)
        self._server_pipmgr.clear_data_assemble_by_port(self.getsockport())
        self._linkmgr.unregister(self)

    def pause_writing(self):
        pass

    def resume_writing(self):
        pass

    def data_received(self, data):
        # print('linkprotocal received:', binascii.crc32(data))
        # print(self, "link order id:", self._order_id)
        self._server_pipmgr.send_data_in_chunk(self.get_requesting_client_port(), data, self._order_id)
        self._order_id += 1

    def write_data(self, data):
        # print('linkprotocal write:', binascii.crc32(data))
        self._transport.write(data)

    def write_eof(self):
        print("write_eof:", self.getsockport())
        # self._transport.write_eof()
        self._is_write_close = True
        self.check_close()

    def getsockname(self):
        return self._sockname

    def getsockport(self):
        return self._sockname[1]

    def get_requesting_client_port(self):
        return self._requestid

    def get_requestid(self):
        return self._requestid

    def close(self):
        if self._transport is not None:
            self._transport.close()
        else:
            self._pendingclose = True
