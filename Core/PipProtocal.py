import asyncio


class PipProtocal(asyncio.Protocol):
    def __init__(self, pipmgr, loop=None):
        self._pipmgr = pipmgr
        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop
        self._writing_paused = False

    def connection_made(self, transport):
        self._transport = transport
        self._pipmgr.register(self)
        self._pipmgr.on_pip_idle(self)

    def connection_lost(self, exc):
        self._pipmgr.unregister(self)

    def pause_writing(self):
        self._writing_paused = True

    def resume_writing(self):
        self._writing_paused = False
        self._pipmgr.on_pip_idle(self)

    def data_received(self, data):
        self._pipmgr.data_received(self, data)

    def is_can_writing(self):
        return not self._writing_paused

    def get_transport(self):
        return self._transport

    def write_data(self, data):
        assert self._writing_paused is False, "Pip is pause writing"
        self._transport.write(data)
